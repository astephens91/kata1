/*console.log("Display the numbers from 1 to 5.");  
for (let counter = 1; counter <= 5; counter++) {
   console.log(counter);
}*/

console.log("Display the numbers from 1 to 20.");
for (let counter = 1; counter <= 20; counter++){
    console.log(counter);
}

console.log("Display the even numbers from 1 to 20.");
for (let counter = 1; counter <=20; counter++){
    if (counter%2!=0) continue;
    console.log(counter);
}

console.log("Display the odd numbers from 1 to 20.");
for (let counter = 1; counter <=20; counter++){
    if (counter%2===0) continue;
    console.log(counter);
}

console.log("Display the multiples of 5 from 1 to 100.");
for (let counter = 1; counter <=100; counter++){
    if (counter%5!=0) continue;
    console.log(counter);
}

console.log("Display the square numbers up to 100.");
for (let counter = 1; counter <=100; counter++){
    let rooted = Math.sqrt(counter);
    if (Number.isInteger(rooted) == false) continue;
        console.log(counter);
}

console.log("Display the numbers counting backwards from 1 to 20.");
for (let counter = 20; counter >= 1; counter--){
    console.log(counter);
}

console.log("Display the even numbers from 20 to 1.");
for (let counter = 20; counter >=1; counter--){
    if (counter%2!=0) continue;
    console.log(counter);
}

console.log("Display the odd numbers from 20 to 1.");
for (let counter = 20; counter >=1; counter--){
    if (counter%2===0) continue;
    console.log(counter);
}

console.log("Display the multiples of 5 from 100 to 1.");
for (let counter = 100; counter >=1; counter--){
    if (counter%5!=0) continue;
    console.log(counter);
}

console.log("Display the square numbers down from 100 to 1.");
for (let counter = 100; counter >=1; counter--){
    let rooted = Math.sqrt(counter);
    if (Number.isInteger(rooted) == false) continue;
        console.log(counter);
}